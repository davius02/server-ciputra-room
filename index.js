const WebSocket = require('ws')

const port = process.env.PORT || 3000;
const wss = new WebSocket.Server({ port: port })

let gameState = []

console.log(`running on ws://127.0.0.1:${port}`)

wss.on('connection', ws => {

  ws.on('message', message => {
    let data = JSON.parse(message)
	if(gameState.filter(el => el.username == data.username).length > 0) {
    	gameState = gameState.map(el => {
    		if(el.username == data.username) return data
    		else return el
    	})
    } else {
		console.log(`new player, id: ${data.username}`)
		gameState.push(data)
	}
	wss.clients.forEach(function each(client) {
       client.send(JSON.stringify(gameState));
    });
	if(data.type==2){
		gameState = gameState.filter(el => el.username != data.username.toString())
	}
	
  })

  ws.on('close', (code, reason) => {
  	gameState = gameState.filter(el => el.username != reason.toString())
  	console.log(gameState)
	
  })
 
})